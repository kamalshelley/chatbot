**BigMouth Chatbot**

Welcome to the BigMouth Chatbot, powered by Google! This simple chatbot allows you to ask questions related to Atom Bank and receive answers. You can open the chatbot in your web browser and start interacting with it.

**Getting Started**

To use the Atom Bank Chatbot, follow these steps:

- Clone or Download the Repository:
    - Clone the GitLab repository containing this HTML code to your local machine.
    - Alternatively, you can download the repository as a ZIP file and extract it to your local directory.
- Open the HTML File:
-    Locate the HTML file named index.html within the repository.
- Open in a Web Browser:
-   Double-click the index.html file to open it in your web browser.
- Start Chatting:
    - Once the page is open, you'll see the chat interface.
    - Type your questions related to Atom Bank in the "Fire your queries here" input field.
- Search and Clear:
    - Click the "Search" button to perform a Google search for your query.
    - Click the "Clear All" button to clear the chat history.

**Common Questions**

Here are some common questions you can ask the Atom Bank Chatbot:

- "Where is Atom Bank located?"
- "Does Atom Bank offer mortgages?"
- "What are the services provided by Atom Bank?"


Feel free to ask any questions you have in mind related to Atom Bank, and the chatbot will provide answers based on a Google search.

**Demo Video**

[Watch the Demo Video](https://gitlab.com/kamalshelley/chatbot/-/raw/main/Demo%20Video.mov?ref_type=heads)


**Chatbot Interface**

![FullPage Screenshots](https://gitlab.com/kamalshelley/chatbot/-/raw/main/Image.png)

**EC2 instance to pull the updatd code:**

- cd /var/www/html/chatbot
- git pull
- git status
- sudo systemctl restart apache2
- sudo systemctl restart nginx




**Author**

KKS
https://gitlab.com/kamalshelley
